// bài 1
// Đầu vào : nhập 3 số bất kì 
// xử lý : tạo 3 biến sothu1,sothu2,sothu3 và 1 biến result để xuất ra màn hình
// so sánh lần lượt các số để xuất ra màn hình
// đầu ra: xuất ra màn hình từ lớn đến bé
function sosanh(){
    var sothu1 = document.getElementById("sothu1").value*1;
    var sothu2 = document.getElementById("sothu2").value*1;
    var sothu3 = document.getElementById("sothu3").value*1;
    var result = document.getElementById("result_bai1").value*1;
    if (sothu1 > sothu2 && sothu1 > sothu3) {
        if (sothu2 > sothu3) {
            document.getElementById("result_bai1").innerHTML=`
            <h1>Sắp xếp theo thứ tự lớn đến bé: ${sothu1} ${sothu2} ${sothu3}</h1>`
        } else {
            document.getElementById("result_bai1").innerHTML=`
            <h1>Sắp xếp theo thứ tự lớn đến bé: ${sothu1} ${sothu3} ${sothu2}</h1>`
        }
      } else if (sothu2 > sothu1 && sothu2 > sothu3) {
        if (sothu1 > sothu3) {
            document.getElementById("result_bai1").innerHTML=`
            <h1>Sắp xếp theo thứ tự lớn đến bé: ${sothu2} ${sothu1} ${sothu3}</h1>`
        } else {
            document.getElementById("result_bai1").innerHTML=`
            <h1>Sắp xếp theo thứ tự lớn đến bé: ${sothu2} ${sothu3} ${sothu1}</h1>`
        }
      } else {
        if (sothu1 > sothu2) {
            document.getElementById("result_bai1").innerHTML=`
            <h1>Sắp xếp theo thứ tự lớn đến bé: ${sothu3} ${sothu2} ${sothu2}</h1>`
        } else {
            document.getElementById("result_bai1").innerHTML=`
            <h1>Sắp xếp theo thứ tự lớn đến bé: ${sothu3} ${sothu2} ${sothu1}</h1>`
        }
      }
}

// bài 2
// Khai báo đối tượng gia đình
var giaDinh = {
    "B": "Chào bố!",
    "M": "Chào mẹ!",
    "A": "Chào anh trai!",
    "E": "Chào em gái!"
  };
// Hàm chào hỏi
function chaoHoi() {
    var ten = document.getElementById("ten").value;
    var thanhVien = ten.toUpperCase().charAt(0);
  
    if (thanhVien in giaDinh) {
      document.getElementById("loiChao").textContent = giaDinh[thanhVien];
    } else {
      document.getElementById("loiChao").textContent = "Chào bạn!";
    }
  }
//   bài 3
// khai báo các biến
// so sánh
// in kết quả chẵn lẻ
function demSoLeVaSoChan() {
    var number1 = document.getElementById("number1").value*1;
    var number2 = document.getElementById("number2").value*1;
    var number3 = document.getElementById("number3").value*1;
  
    var soLe = 0;
    var soChan = 0;
  
    if (number1 % 2 === 0) {
      soChan++;
    } else {
      soLe++;
    }
  
    if (number2 % 2 === 0) {
      soChan++;
    } else {
      soLe++;
    }
  
    if (number3 % 2 === 0) {
      soChan++;
    } else {
      soLe++;
    }
  
    var ketQua = "Số lẻ: " + soLe + ", Số chẵn: " + soChan;
    document.getElementById("ketQua").textContent = ketQua;
  }
// bài 4
// khai báo các biến
// so sánh các cạnh và xử lý
// xuất kết quả
function xacDinhTamGiac() {
    var canh1 = document.getElementById("canh1").value*1;
    var canh2 = document.getElementById("canh2").value*1;
    var canh3 = document.getElementById("canh3").value*1;

    if (canh1 === canh2 && canh2 === canh3) {
      document.getElementById("tamgiac").textContent = "Tam giác đều";
    } else if (canh1 === canh2 || canh1 === canh3 || canh2 === canh3) {
      document.getElementById("tamgiac").textContent = "Tam giác cân";
    } else if (
      canh1 ** 2 === canh2 ** 2 + canh3 ** 2 ||
      canh2 ** 2 === canh1 ** 2 + canh3 ** 2 ||
      canh3 ** 2 === canh1 ** 2 + canh2 ** 2
    ) {
      document.getElementById("tamgiac").textContent = "Tam giác vuông (theo định lý Pythagoras)";
    } else {
      document.getElementById("tamgiac").textContent = "Không phải tam giác đặc biệt";
    }
  }